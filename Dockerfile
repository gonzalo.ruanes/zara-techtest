FROM maven as builder
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN mvn package -DskipTests=true

FROM openjdk:11.0.5-jre-slim
COPY --from=builder /usr/src/app/target/*.jar /app/zara-test-app.jar
WORKDIR /app
CMD ["java", "-jar", "zara-test-app.jar"]

RUN groupadd -r userbuild && useradd --no-log-init -r -g userbuild userbuild
USER userbuild