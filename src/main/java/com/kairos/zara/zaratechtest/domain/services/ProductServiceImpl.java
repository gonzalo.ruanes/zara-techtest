package com.kairos.zara.zaratechtest.domain.services;

import com.kairos.zara.zaratechtest.domain.model.DomainProduct;
import com.kairos.zara.zaratechtest.domain.ports.in.ProductService;
import com.kairos.zara.zaratechtest.domain.ports.out.ProductDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public class ProductServiceImpl implements ProductService {

  @Autowired
  ProductDatabaseService productDatabaseService;

  @Override
  public DomainProduct createProduct(DomainProduct product) {
    return productDatabaseService.createProduct(product);
  }

  @Override
  public DomainProduct getProduct(String id) {
    return productDatabaseService.getProduct(id);
  }

  @Override
  public Page<DomainProduct> getAllDomainProducts(Pageable pageable) {
    return productDatabaseService.getAllDomainProducts(pageable);
  }
}
