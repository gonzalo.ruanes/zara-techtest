package com.kairos.zara.zaratechtest.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DomainProduct {

  private String id;

  private String productName;

  private String description;

  private double price;

}
