package com.kairos.zara.zaratechtest.domain.ports.out;

import com.kairos.zara.zaratechtest.domain.model.DomainProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductDatabaseService {

  DomainProduct createProduct(DomainProduct product);

  DomainProduct getProduct(String id);

  Page<DomainProduct> getAllDomainProducts(Pageable pageable);

}
