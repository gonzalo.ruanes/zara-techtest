package com.kairos.zara.zaratechtest.infrastructure.adapters.out;

import com.kairos.zara.zaratechtest.domain.model.DomainProduct;
import com.kairos.zara.zaratechtest.domain.ports.out.ProductDatabaseService;
import com.kairos.zara.zaratechtest.infrastructure.model.exceptions.ProductNotFoundException;
import com.kairos.zara.zaratechtest.infrastructure.repositories.ProductRepository;
import com.kairos.zara.zaratechtest.infrastructure.repositories.model.ProductModel;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ProductDatabaseAdapter implements ProductDatabaseService {

  private ProductRepository productRepository;

  private MapperFacade mapper;

  @Override
  public DomainProduct createProduct(DomainProduct product) {
    ProductModel productModel = mapper.map(product,ProductModel.class);
    productModel.setId(null);
    ProductModel insertedProduct = productRepository.save(productModel);
    return mapper.map(insertedProduct,DomainProduct.class);
  }

  @Override
  public DomainProduct getProduct(String id) {
    return productRepository.findById(id)
        .map((retrievedProduct) -> mapper.map(retrievedProduct,DomainProduct.class))
        .orElseThrow(ProductNotFoundException::new);
  }

  @Override
  public Page<DomainProduct> getAllDomainProducts(Pageable pageable) {
    return productRepository.findAll(pageable)
        .map((product -> mapper.map(product,DomainProduct.class)));
  }
}
