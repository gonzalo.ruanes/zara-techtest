package com.kairos.zara.zaratechtest.infrastructure.controllers.impl;

import com.kairos.zara.zaratechtest.config.log.Logger;
import com.kairos.zara.zaratechtest.infrastructure.adapters.in.ProductServiceAdapter;
import com.kairos.zara.zaratechtest.infrastructure.controllers.ProductController;
import com.kairos.zara.zaratechtest.infrastructure.model.ProductDTO;
import javax.websocket.server.PathParam;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/products")
public class ProductControllerImpl implements ProductController {

  @Autowired
  private ProductServiceAdapter productServiceAdapter;

  @Autowired
  private MapperFacade mapper;

  @Override
  @Logger
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ProductDTO createProduct(ProductDTO productDTO) {
    return productServiceAdapter.createProduct(productDTO);
  }

  @Override
  @Logger
  @GetMapping
  public Page<ProductDTO> getProducts(Pageable pageable) {
    return productServiceAdapter.getAllProducts(pageable);
  }

  @Override
  @Logger
  @GetMapping("/{id}")
  public ProductDTO getProduct(@PathVariable("id") String id) {
    return productServiceAdapter.getProduct(id);
  }
}
