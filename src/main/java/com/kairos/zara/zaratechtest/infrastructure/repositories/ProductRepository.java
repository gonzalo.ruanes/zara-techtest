package com.kairos.zara.zaratechtest.infrastructure.repositories;

import com.kairos.zara.zaratechtest.infrastructure.repositories.model.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<ProductModel, String> {

}
