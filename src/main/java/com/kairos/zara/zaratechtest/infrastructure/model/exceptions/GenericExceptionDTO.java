package com.kairos.zara.zaratechtest.infrastructure.model.exceptions;

import java.util.UUID;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class GenericExceptionDTO {

  private int status;

  private String message;

  private UUID code;

}
