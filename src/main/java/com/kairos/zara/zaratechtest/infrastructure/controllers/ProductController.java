package com.kairos.zara.zaratechtest.infrastructure.controllers;

import com.kairos.zara.zaratechtest.infrastructure.model.ProductDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import springfox.documentation.annotations.ApiIgnore;

@Api
public interface ProductController {

  @ApiOperation("Create a product")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Product created successfully, product data returned on response body"),
      @ApiResponse(code = 400, message = "Invalid product data")})
  ProductDTO createProduct(@RequestBody ProductDTO policyDTO);

  @ApiOperation(value = "Retrieve the products list")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
          value = "Results page you want to retrieve"),
      @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
          value = "Number of records per page")})
  Page<ProductDTO> getProducts(@ApiIgnore Pageable pageable);

  @ApiOperation("Retrieve a product")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Product successfully retrieved"),
      @ApiResponse(code = 404, message = "Product not found")})
  ProductDTO getProduct(@ApiParam(value = "id", required = true) String id);

}
