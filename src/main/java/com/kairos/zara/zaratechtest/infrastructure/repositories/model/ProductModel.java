package com.kairos.zara.zaratechtest.infrastructure.repositories.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductModel {

  @Id
  private String id;

  private String productName;

  private String description;

  private double price;

}
