package com.kairos.zara.zaratechtest.infrastructure.adapters.in;

import com.kairos.zara.zaratechtest.domain.model.DomainProduct;
import com.kairos.zara.zaratechtest.domain.ports.in.ProductService;
import com.kairos.zara.zaratechtest.infrastructure.model.ProductDTO;
import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ProductServiceAdapter {

  private ProductService productService;

  private MapperFacade mapper;

  public ProductDTO createProduct(ProductDTO productDTO) {
    DomainProduct createdProduct = productService.createProduct(mapper.map(productDTO, DomainProduct.class));
    return mapper.map(createdProduct, ProductDTO.class);
  }

  public ProductDTO getProduct(String id) {
    return mapper.map(productService.getProduct(id),ProductDTO.class);
  }

  public Page<ProductDTO> getAllProducts(Pageable pageable) {
    return productService.getAllDomainProducts(pageable).map(
        (product) -> mapper.map(product,ProductDTO.class)
    );
  }
}
