package com.kairos.zara.zaratechtest.infrastructure.model.exceptions;

public class ProductNotFoundException extends RuntimeException {

  private static final String PRODUCT_NOT_FOUND = "Product not found";

  public ProductNotFoundException(){
    super(PRODUCT_NOT_FOUND);
  }

}
