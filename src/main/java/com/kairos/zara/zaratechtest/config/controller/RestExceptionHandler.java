package com.kairos.zara.zaratechtest.config.controller;

import com.kairos.zara.zaratechtest.infrastructure.model.exceptions.GenericExceptionDTO;
import com.kairos.zara.zaratechtest.infrastructure.model.exceptions.ProductNotFoundException;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public GenericExceptionDTO defaultException(Exception e) {
    return buildExceptionDTO(e,HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(value = ProductNotFoundException.class)
  public GenericExceptionDTO handleNotFoundException(Exception ex) {
    return buildExceptionDTO(ex, HttpStatus.NOT_FOUND);
  }

  private GenericExceptionDTO buildExceptionDTO(Exception ex, HttpStatus status) {
    UUID code = UUID.randomUUID();
    log.error("An error has occurred - {}", code);
    log.info("message - {}", ex.getMessage());

    return GenericExceptionDTO.builder()
        .code(code)
        .message(ex.getMessage())
        .status(status.value())
        .build();
  }

}
