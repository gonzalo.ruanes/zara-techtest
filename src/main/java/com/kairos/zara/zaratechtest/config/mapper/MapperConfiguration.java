package com.kairos.zara.zaratechtest.config.mapper;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class MapperConfiguration {

  @Bean
  @Autowired
  public MapperFacade getMapperFacade(List<CustomConverter> customConverters) {
    MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
    customConverters.stream()
        .peek(converter -> log.info(String.format("Registering converter from class %s to class %s", converter.getAType().getName(), converter.getBType().getName())))
        .forEach(mapperFactory.getConverterFactory()::registerConverter);
    return mapperFactory.getMapperFacade();
  }

}
