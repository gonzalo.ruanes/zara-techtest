package com.kairos.zara.zaratechtest.infrastructure.adapters.in;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.kairos.zara.zaratechtest.domain.model.DomainProduct;
import com.kairos.zara.zaratechtest.domain.ports.in.ProductService;
import com.kairos.zara.zaratechtest.domain.ports.out.ProductDatabaseService;
import com.kairos.zara.zaratechtest.domain.services.ProductServiceImpl;
import com.kairos.zara.zaratechtest.infrastructure.model.ProductDTO;
import java.util.Collections;
import java.util.UUID;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceAdapterTest {

  @Mock
  ProductService productService;

  ProductServiceAdapter productServiceAdapter;

  MapperFacade mapper = new DefaultMapperFactory.Builder().build().getMapperFacade();

  ProductDTO newProduct = ProductDTO.builder()
      .description(UUID.randomUUID().toString())
      .productName(UUID.randomUUID().toString())
      .price(99.99)
      .build();

  DomainProduct createdProduct = mapper.map(newProduct,DomainProduct.class);

  @Before
  public void setUp(){
    productServiceAdapter = new ProductServiceAdapter(productService,mapper);

    when(productService.createProduct(any(DomainProduct.class))).thenReturn(createdProduct);
    when(productService.getProduct(any(String.class))).thenReturn(createdProduct);
    when(productService.getAllDomainProducts(any(
        Pageable.class))).thenReturn(new PageImpl<>(
        Collections.singletonList(createdProduct)));
  }

  @Test
  public void shouldCreateProduct(){
    assertEquals(productServiceAdapter.createProduct(newProduct),newProduct);
  }

  @Test
  public void shouldRetrieveAllProducts(){
    Page<ProductDTO> expectedResult = new PageImpl<>(Collections.singletonList(newProduct));
    assertEquals(productServiceAdapter.getAllProducts(Pageable.unpaged()),expectedResult);
  }

  @Test
  public void shouldFindProductById(){
    assertEquals(productServiceAdapter.getProduct("1234"),newProduct);
  }

}
