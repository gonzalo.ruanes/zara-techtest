package com.kairos.zara.zaratechtest.infrastructure.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kairos.zara.zaratechtest.infrastructure.model.ProductDTO;
import com.kairos.zara.zaratechtest.infrastructure.repositories.ProductRepository;
import com.kairos.zara.zaratechtest.infrastructure.repositories.model.ProductModel;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductControllerIntegrationTest {

  private static final String REQUEST_MAPPING = "/products";
  private final ObjectMapper objectMapper = new ObjectMapper();

  @Autowired
  MockMvc mockMvc;

  @Autowired
  ProductRepository productRepository;

  Iterable<ProductModel> productList;

  @Before
  public void setUp(){
    productRepository.deleteAll();
    productList = productRepository.saveAll(getInitialProductsIterable());
  }

  @Test
  public void shouldCreateProduct() throws Exception {
    ProductDTO newProduct = ProductDTO.builder()
        .productName("Zapatillas")
        .description("Para Running")
        .price(25.99)
        .build();

    ResultActions result = mockMvc.perform(post(REQUEST_MAPPING)
        .content(objectMapper.writeValueAsString(newProduct))
        .contentType(MediaType.APPLICATION_JSON)
    );

    result.andExpect(status().isCreated())
        .andExpect(jsonPath("$.productName").value(newProduct.getProductName()))
        .andExpect(jsonPath("$.description").value(newProduct.getDescription()))
        .andExpect(jsonPath("$.price").value(newProduct.getPrice()));

  }

  @Test
  public void shouldRetrieveAllProducts() throws Exception {
    ResultActions result = mockMvc.perform(get(REQUEST_MAPPING)
    );

    result.andExpect(status().isOk())
    .andExpect(jsonPath("$.totalElements").value(3));

  }

  @Test
  public void shouldFindProductById() throws Exception {
    ProductModel expectedProduct = productList.iterator().next();
    ResultActions result = mockMvc.perform(get(REQUEST_MAPPING + "/{id}",
        expectedProduct.getId()));

    result.andExpect(status().isOk())
        .andExpect(jsonPath("$.productName").value(expectedProduct.getProductName()))
        .andExpect(jsonPath("$.description").value(expectedProduct.getDescription()))
        .andExpect(jsonPath("$.price").value(expectedProduct.getPrice()));
  }

  @Test
  public void shouldReturnErrorWhenFindingNonExistentProduct() throws Exception {
    ResultActions result = mockMvc.perform(get(REQUEST_MAPPING + "/{id}",
        "12345"));

    result.andExpect(status().isNotFound());
  }

  private Iterable<ProductModel> getInitialProductsIterable(){
    return Arrays.asList(
        ProductModel.builder()
            .productName("Camiseta")
            .description("Muy Bonita")
            .price(10.99)
            .build(),
        ProductModel.builder()
            .productName("Pantalon")
            .description("Muy Bonito")
            .price(20.99)
            .build(),
        ProductModel.builder()
            .productName("Gafas de sol")
            .description("A tope de ciclos")
            .price(18.99)
            .build());
  }

}
