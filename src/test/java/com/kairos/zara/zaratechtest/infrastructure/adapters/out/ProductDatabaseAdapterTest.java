package com.kairos.zara.zaratechtest.infrastructure.adapters.out;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.kairos.zara.zaratechtest.domain.model.DomainProduct;
import com.kairos.zara.zaratechtest.infrastructure.model.exceptions.ProductNotFoundException;
import com.kairos.zara.zaratechtest.infrastructure.repositories.ProductRepository;
import com.kairos.zara.zaratechtest.infrastructure.repositories.model.ProductModel;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@RunWith(MockitoJUnitRunner.class)
public class ProductDatabaseAdapterTest {

  @Mock
  ProductRepository productRepository;

  ProductDatabaseAdapter productDatabaseAdapter;

  MapperFacade mapper = new DefaultMapperFactory.Builder().build().getMapperFacade();

  ProductModel createdProduct = ProductModel.builder()
      .id(UUID.randomUUID().toString())
      .description(UUID.randomUUID().toString())
      .productName(UUID.randomUUID().toString())
      .price(99.99)
      .build();

  DomainProduct domainProduct = mapper.map(createdProduct,DomainProduct.class);


  @Before
  public void setUp(){
    productDatabaseAdapter = new ProductDatabaseAdapter(productRepository,mapper);

    when(productRepository.save(any(ProductModel.class))).thenReturn(createdProduct);
    when(productRepository.findById(any(String.class))).thenReturn(Optional.of(createdProduct));
    when(productRepository.findAll(any(
        Pageable.class))).thenReturn(new PageImpl<ProductModel>(
        Collections.singletonList(createdProduct)));
  }

  @Test
  public void shouldCreateProduct(){
    assertEquals(productDatabaseAdapter.createProduct(domainProduct),domainProduct);
  }

  @Test
  public void shouldRetrieveAllProducts(){
    Page<DomainProduct> expectedResult = new PageImpl<>(Collections.singletonList(domainProduct));
    assertEquals(productDatabaseAdapter.getAllDomainProducts(Pageable.unpaged()),expectedResult);
  }

  @Test
  public void shouldFindProductById(){
    assertEquals(productDatabaseAdapter.getProduct("1234"),domainProduct);
  }

  @Test(expected = ProductNotFoundException.class)
  public void shouldThrowExceptionWhenRetrievingNonExistentId(){
    when(productRepository.findById(any(String.class))).thenReturn(Optional.empty());
    productDatabaseAdapter.getProduct("1234");
  }

}
