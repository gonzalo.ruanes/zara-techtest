package com.kairos.zara.zaratechtest.domain.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.kairos.zara.zaratechtest.domain.model.DomainProduct;
import com.kairos.zara.zaratechtest.domain.ports.out.ProductDatabaseService;
import java.util.Collections;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {

  @Mock
  ProductDatabaseService productDatabaseService;

  @InjectMocks
  ProductServiceImpl productService;

  DomainProduct domainProduct = DomainProduct.builder()
      .description(UUID.randomUUID().toString())
      .productName(UUID.randomUUID().toString())
      .price(99.99)
      .build();

  @Before
  public void setUpMocks(){
    when(productDatabaseService.createProduct(any(DomainProduct.class))).thenReturn(domainProduct);
    when(productDatabaseService.getProduct(any(String.class))).thenReturn(domainProduct);
    when(productDatabaseService.getAllDomainProducts(any(Pageable.class))).thenReturn(new PageImpl<DomainProduct>(
        Collections.singletonList(domainProduct)));
  }

  @Test
  public void shouldCreateProduct(){
    assertEquals(productService.createProduct(domainProduct),domainProduct);
  }

  @Test
  public void shouldRetrieveAllProducts(){
    Page<DomainProduct> expectedResult = new PageImpl<>(Collections.singletonList(domainProduct));
    assertEquals(productService.getAllDomainProducts(Pageable.unpaged()),expectedResult);
  }

  @Test
  public void shouldFindProductById(){
    String id = "1234";
    assertEquals(productService.getProduct(id),domainProduct);
  }

}
