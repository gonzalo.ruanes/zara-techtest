# zara-techtest

Tech Test for Zara 
- Author: Gonzalo Ruanes Gil

## Swagger UI API
The app provides a documented __Swagger API__ for further information and testing.
> http://localhost:8080/swagger-ui.html

## Postman
For further testing you can import the collection to Postman:

- File/Import/Import from link

And insert this: http://localhost:8080/v2/api-docs

## Docker image

In order to create a docker image for the project you can run the command ```docker build``` 
on the project's directory.

## Docker Compose for local and development environment

You can execute the docker-compose file located in the project folder in order to deploy 
the application and database containers

```console
docker-compose up -d
```

